package com.sigmotoa.codes.workshop;

/**
 * @author sigmotoa
 * <p>
 * This class contains some popular brain games with numbers
 */
public class Agility {
    private static int Sum;

    //bigger than

    //Show if the first number is bigger than the second
    public static boolean biggerThan(String numA, String numB) {

        double numIntA = Double.parseDouble(numA);

        double numIntB = Double.parseDouble(numB);

        if (numIntA > numIntB) {

            return true;
        } else {
            return false;
        }
    }

    //Sort from bigger the numbers an show in array
    public static int[] order(int numA, int numB, int numC, int numD, int numE) {
        int[] numbers = new int[5];
        numbers[0] = numA;
        numbers[1] = numB;
        numbers[2] = numC;
        numbers[3] = numD;
        numbers[4] = numE;

        int temp;
        for (int i = 0; i < numbers.length-1; i++) {
            for (int j = 0; j < numbers.length-i-1; j++) {
                if (numbers[j+1] < numbers[j]) {
                    temp = numbers[j+1];
                    numbers[j+1] = numbers[j];
                    numbers[j] = temp;
                }
            }
        }
        return numbers;
    }


    //Look for the smaller number of the array
    public static double smallerThan(double array[]) {

        double smaller, temp;
        int pos;
        for (int i = 0; i < array.length - 1; i++) {
            smaller = array[i];
            pos = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < smaller) {
                    smaller = array[j];
                    pos = j;
                }
            }
            if (pos != i) {
                temp = array[i];
                array[i] = array[pos];
                array[pos] = temp;
            }
        }
        return array[0];
    }

    //Palindrome number is called in Spanish capicúa
    //The number is palindrome
    public static boolean palindromeNumber(Integer numA) {

        long rest, numInverse, subtract;



            rest = numA;
            numInverse = 0;
            subtract = 0;

            while (rest != 0) {
                subtract = rest % 10;
                numInverse = numInverse * 10 + subtract;
                rest = rest / 10;
            }

            if (numInverse == numA) {

                return true;
            }
            else {

                return false;
            }

    }

    //the word is palindrome
    //public static boolean palindromeWord(String word)
    public static boolean palindromeWord(String word) {
        String reverse = "";

        int length = word.length();

        for (int i = length - 1; i >= 0; i--) {
            reverse = reverse + word.charAt(i);
        }

        if (word.equals(reverse)) {
            return true;
        } else {
            return false;
        }
    }

    //Show the factorial number for the parameter
    public static int factorial(int numA) {
        int i, e;
        e = 1;

        for (i = 2; i <= numA; i++) {
            e = e * i;

        }
        return e;

    }

    //is the number odd
    public static boolean isOdd(byte numA) {
        if (numA % 2 != 0) {
            return true;
        } else {

            return false;
        }

    }

    //is the number prime
    public static boolean isPrimeNumber(int numA) {

        //int temp = 2;
        //boolean prime = true;
        //while ((prime) && (temp != numA)) {
        //    if (numA % temp == 0)
        //        prime = false;
        //    temp++;
        //}
        for (int i = 2; i < numA; i++) {
            if (numA % i == 0) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    //is the number even
    public static boolean isEven(byte numA) {
        if (numA % 2 != 0) {
            return false;
        } else {

            return true;
        }
    }

    //is the number perfect
    public static boolean isPerfectNumber(int numA) {
        int suma;
        suma = 0;
        for (int i = 1; i < numA; i++) {
            if (numA % i == 0) {
                suma = suma + i;
            }
        }
        if (suma == numA) {
            return true;
        } else {
            return false;
        }
    }

    //Return an array with the fibonacci sequence for the requested number
    public static int[] fibonacci(int numA) {

        return new int[0];
    }

    //how many times the number is divided by 3
    public static int timesDividedByThree(int numA) {

        int numB, resto, counter;

        numB = 3;

        resto = numA % numB;
        counter = numA / numB;
        if (resto == 0) {

            return counter;
        } else {

            return counter;
        }

    }

    //The game of fizzbuzz
    public static String fizzBuzz(int numA)



        /**
         * If number is divided by 3, show fizz
         * If number is divided by 5, show buzz
         * If number is divided by 3 and 5, show fizzbuzz
         * in other cases, show the number
         */
        {
            String fb;
            if (numA%3==0 && numA%5==0){
                fb = "FizzBuzz";
                return fb;
            }
            else{
                if(numA%3==0){
                    fb = "Fizz";
                    return fb;
                }
                else{
                    if(numA%5==0){
                        fb = "Buzz";
                        return fb;
                    }
                    else{
                        //fb = ""+numA;
                        fb = new String(String.valueOf(numA));
                        return fb;
                    }
                }
            }
        }
    }
