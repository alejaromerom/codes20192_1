package com.sigmotoa.codes.workshop;

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
public class Convertion {

    //convert of units of Metric System

    //Km to metters
    public static int kmToM1(double km) {

        int kmconversion = (int) km / 1000;

        return kmconversion;
    }

    //Km to metters
    public static double kmTom(double km) {

        double kmconversion = km / 1000;
        return kmconversion;
    }

    //Km to cm
    public static double kmTocm(double km) {

        double kmconversion = km * 100000;

        return kmconversion;
    }

    //milimetters to metters
    public static double mmTom(int mm)

    {
        double milimetrosconversion=mm/1000;

        return milimetrosconversion;
    }
//convert of units of U.S Standard System

    //convert miles to foot
    public static double milesToFoot(double miles)

    {

        double piesconversion=miles*5280;

        return piesconversion;
    }

    //convert yards to inches
    public static int yardToInch(int yard)
    {

        int pulgadasconversion=yard*36;

        return pulgadasconversion;
    }

    //convert inches to miles
    public static double inchToMiles(double inch)
    {

        double millasconversion=inch/63360;

        return millasconversion;
    }

    //convert foot to yards
    public static int footToYard(int foot)

    {
       int piesconversion=foot/3;
        return piesconversion;
    }

//Convert units in both systems

    //convert Km to inches
    public static double kmToInch(String km)

{
    double kMapulgadas= Integer.parseInt(km)*39370.079;

    return kMapulgadas;
}

//convert milimmeters to foots
    public static double mmToFoot(String mm)
    {

        double mmtofootconversion=Integer.parseInt(mm)*304.8;

        return mmtofootconversion;
    }
//convert yards to cm    
    public static double yardToCm(String yard)
    {

       double yardtocmconversion=Integer.parseInt(yard)*91.44;

        return yardtocmconversion;
    }


}
