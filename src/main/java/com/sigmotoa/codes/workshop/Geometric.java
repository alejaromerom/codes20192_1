package com.sigmotoa.codes.workshop;
/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Geometric Exercises
 */


import static java.lang.Math.*;

public class Geometric {

//Calculate the area for a square using one side
public static int squareArea(int side)


{
  int area=side*side;

    return area;
}

//Calculate the area for a square using two sides
public static int squareArea(int sidea, int sideb)
{

   int area=sidea*sideb;

        return area;
}

//Calculate the area of circle with the radius
public static double circleArea(double radius)


{

    double area=Math.PI*Math.pow(radius,2);

    return area;
}


//Calculate the perimeter of circle with the diameter
public static double circlePerimeter(int diameter)
{

    double perimeter=Math.PI*diameter;

    return perimeter;
}

//Calculate the perimeter of square with a side
public static double squarePerimeter(double side)
{

    double perimeter=4*side;

    return perimeter;
}

//Calculate the volume of the sphere with the radius
public static double sphereVolume(double radius)
{

    double volume=4/3*Math.PI*Math.pow(radius,3);
    return volume;
}

//Calculate the area of regular pentagon with one side
public static float pentagonArea(int side)
{
    float area = (float) (5*(Math.pow(side,2))/4*Math.tan(Math.PI/5));
    return area;
}

//Calculate the Hypotenuse with two cathetus
public static double calculateHypotenuse(double catA, double catB)
{

    double hypotenuse=Math.sqrt(Math.pow(catA,2)*Math.pow(catB,2));

    return hypotenuse;
}

}
